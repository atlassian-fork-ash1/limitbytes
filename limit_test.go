package limitbytes

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestLimitBytesFailureUnknownLength(t *testing.T) {
	var next = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		_, e := ioutil.ReadAll(r.Body)
		switch e.(type) {
		case nil:
			w.WriteHeader(http.StatusOK)
		case ErrTooLarge:
			w.WriteHeader(http.StatusRequestEntityTooLarge)
		default:
			w.WriteHeader(http.StatusBadRequest)
		}
	})
	var callback = func(w http.ResponseWriter, r *http.Request, e error) {
		t.Fatal("Callback function must not be called.")
	}
	var w = httptest.NewRecorder()
	var r, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`More than 1 byte`))
	r.ContentLength = -1

	var middleware = New(1, callback)
	middleware(next).ServeHTTP(w, r)

	if w.Code != http.StatusRequestEntityTooLarge {
		t.Fatalf("Expected status %d but got %d", http.StatusRequestEntityTooLarge, w.Code)
	}
}

func TestLimitBytesFailureKnownLength(t *testing.T) {
	var next = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.Fatalf("Next http handler must not be called")
	})
	var callback = func(w http.ResponseWriter, r *http.Request, e error) {
		switch e.(type) {
		case nil:
			w.WriteHeader(http.StatusOK)
		case ErrTooLarge:
			w.WriteHeader(http.StatusRequestEntityTooLarge)
		default:
			w.WriteHeader(http.StatusBadRequest)
		}
	}
	var w = httptest.NewRecorder()
	var r, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`More than 1 byte`))

	var middleware = New(1, callback)
	middleware(next).ServeHTTP(w, r)

	if w.Code != http.StatusRequestEntityTooLarge {
		t.Fatalf("Expected status %d but got %d", http.StatusRequestEntityTooLarge, w.Code)
	}
}

func TestLimitBytesFailureNoCallback(t *testing.T) {
	var next = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.Fatalf("Next http handler must not be called")
	})
	var w = httptest.NewRecorder()
	var r, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`More than 1 byte`))

	var middleware = New(1, nil)
	middleware(next).ServeHTTP(w, r)

	if w.Code != http.StatusRequestEntityTooLarge {
		t.Fatalf("Expected status %d but got %d", http.StatusRequestEntityTooLarge, w.Code)
	}
}

func TestLimitBytesSuccess(t *testing.T) {
	var next = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		_, e := ioutil.ReadAll(r.Body)
		switch e.(type) {
		case nil:
			w.WriteHeader(http.StatusOK)
		case ErrTooLarge:
			w.WriteHeader(http.StatusRequestEntityTooLarge)
		default:
			w.WriteHeader(http.StatusBadRequest)
		}
	})
	var callback = func(w http.ResponseWriter, r *http.Request, e error) {
		t.Fatal("Callback function must not be called.")
	}
	var w = httptest.NewRecorder()
	var r, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBufferString(`Less than 1024 bytes`))
	var middleware = New(1024, callback)
	middleware(next).ServeHTTP(w, r)

	if w.Code != http.StatusOK {
		t.Fatalf("Expected status %d but got %d", http.StatusOK, w.Code)
	}
}
